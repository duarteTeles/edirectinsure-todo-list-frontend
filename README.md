# EdirectInsure TODO App Frontend

This is the main README for the Edirect TODO app - frontend. It contains all the necessary instructions for anyone to setup the frontend of the Todo App.

## Getting Started

THe following sections describe how anyone can get this project setup and running. This App was created with **create react app**.

### Prerequisites

The following is required to be installed on your machine:

1. Node version 12.13.0 or greater
2. npm version 6.12.0 or greater
3. yarn 1.22.4

**NOTE**: The backend of the todo app must be up and running. The frontend will run, by default, on port 3001
and the backend on port 3000. PLease follow this link to setup the backend of the todo app:
https://bitbucket.org/duarteTeles/edirectinsure-todo-list-backend/src/master/


### Setup and Installation

To setup up this project, first clone the project:

```
git clone https://duarteTeles@bitbucket.org/duarteTeles/edirectinsure-todo-list-frontend.git
```

Then navigate to the project folder and install all the required dependencies:

```
cd edirectinsure-todo-list-frontend. && yarn install
```


Start the React app (by default it runs on PORT 3001):

```
yarn start
```




## Folder Structure


* **public**: contains all the public files   
* **src/auth**: contains all the authentication routes
* **src/components**: contains all the components in this app
* **pages/main and pages/home**: main files used in this app
* **services**: contains all the logic to fetch data from the todo app - backend


## Built With

* [ReactJS](https://reactjs.org/) - A JavaScript library for building user interfaces
## Authors

* **Duarte Teles** - [Bitbucket](https://bitbucket.org/%7B65e86de9-c623-412d-8584-596b396bbb67%7D/)

# Improvements/ future work
1. Have a Docker container running the frontend of the TODO App
2. Run and develop unit tests with [Jest](https://jestjs.io/)

# What is NOT done (due to lack of time, only in the frontend)
1. Edit user projects
2. Edit and add new tasks for a user project
3. Marking tasks as finished

**NOTE**: These requirements are NOT done in the frontend, but completed in the backend.


## Acknowledgments

I would like to thank Edirect Insure for this amazing opportunity to show my value. I have had a lot of fun developing this project.

