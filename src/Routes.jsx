import {
  BrowserRouter as Router, Route, Switch,
} from 'react-router-dom';

import React from 'react';
import PageNotFound from './pages/errors/PageNotFound';
import Home from './pages/Home';
import Login from './auth/Login';
import Register from './auth/Register';

class Routes extends React.Component {
  render() {
    return (
      <Router>

        <Switch>
          <Route exact path="/" component={Home} />


          {/* <Route */}
          {/*    exact */}
          {/*    path="/auth/account" */}
          {/*    component={MyAccount} */}
          {/*    history={browserHistory} */}
          {/* /> */}


          <Route
            exact
            path="/auth/login"
            component={Login}
          />
          <Route
            exact
            path="/auth/register"
            component={Register}
          />

          <PageNotFound />
        </Switch>
      </Router>
    );
  }
}

export default Routes;
