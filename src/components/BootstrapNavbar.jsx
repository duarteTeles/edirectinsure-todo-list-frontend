import React from 'react';
import cookie from 'react-cookies';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  useParams,
} from 'react-router-dom';
import {
  Navbar, Nav, NavDropdown,
} from 'react-bootstrap';


class BootstrapNavbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoggedIn: false,
      firstName: '',
      lastName: '',
    };
  }

  handleLogout() {
    cookie.remove('userData', { path: '/' });
    alert('Logging out...');
    this.setState({ isLoggedIn: false });
  }

  componentDidMount() {
    if (cookie.load('userData') !== undefined) {
      this.setState({ isLoggedIn: true, firstName: cookie.load('userData').firstName, lastName: cookie.load('userData').lastName });
    }
  }


  render() {
    return (
      <div>
        <div className="row">
          <div className="col-md-12">
            <Router>
              <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <Navbar.Brand href="/">EdirectInsure TODO List</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                  {!this.state.isLoggedIn ? (
                    <Nav className="mr-auto">
                      <Nav.Link href="/auth/login">Login</Nav.Link>
                      <Nav.Link href="/auth/register">Register</Nav.Link>
                    </Nav>
                  ) : null}
                  {this.state.isLoggedIn ? (
                    <Nav className="ml-auto">
                      <NavDropdown title={`${this.state.firstName} ${this.state.lastName}`}>
                        <NavDropdown.Item href="/" onClick={this.handleLogout}>Logout</NavDropdown.Item>
                      </NavDropdown>
                    </Nav>
                  ) : null}
                </Navbar.Collapse>
              </Navbar>
              <br />
            </Router>
          </div>
        </div>
      </div>
    );
  }
}

export default BootstrapNavbar;
