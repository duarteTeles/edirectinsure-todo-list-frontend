import React from 'react';
import { render } from 'react-dom';
import { withFormik } from 'formik';
import * as Yup from 'yup';
import axios from 'axios';
import cookie from 'react-cookies';
import BootstrapNavbar from '../components/BootstrapNavbar';
import { login, getAuthenticatedUserDetails } from '../services/todoListAPI';


class MyForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <form onSubmit={this.props.handleSubmit}>
        <h1>Login </h1>
        <div className="form-group">
          <label
            htmlFor="email"
            style={{ display: 'block', marginTop: '.5rem' }}
          >
            Email
          </label>
          <input
            id="email"
            placeholder="Enter your email"
            type="text"
            value={this.props.values.email}
            onChange={this.props.handleChange}
            onBlur={this.props.handleBlur}
            className="form-control"
          />
          {this.props.errors.email
                    && this.props.touched.email && (
                    <div style={{ color: 'red', marginTop: '.5rem' }}>
                      {this.props.errors.email}
                    </div>
          )}
        </div>
        <div className="form-group">
          <label
            htmlFor="password"
            style={{ display: 'block', marginTop: '.5rem' }}
          >
            Password
          </label>
          <input
            id="password"
            placeholder="Enter your password"
            type="password"
            value={this.props.values.password}
            onChange={this.props.handleChange}
            onBlur={this.props.handleBlur}
            className="form-control"
          />
          {this.props.errors.password
                    && this.props.touched.password && (
                    <div style={{ color: 'red', marginTop: '.5rem' }}>
                      {this.props.errors.password}
                    </div>
          )}
        </div>
        <button
          type="button"
          className="outline btn btn-default"
          onClick={this.props.handleReset}
          disabled={!this.props.dirty || this.props.isSubmitting}
        >
          Reset
        </button>
        <button
          type="submit"
          className="btn btn-primary"
          disabled={this.props.isSubmitting}
        >
          Submit
        </button>
      </form>
    );
  }
}

const MyLoginEnhancedForm = withFormik({
  mapPropsToValues: (props) => ({
    email: '',
    password: '',
  }),
  validationSchema: Yup.object().shape({
    email: Yup.string().email('Invalid email address')
      .required('Email is required'),
    password: Yup.string().required('Password is required').min(6).max(20),
  }),

  handleSubmit: async (values, { setSubmitting, props }, event) => {
    const { history } = props;


    const {
      email, password,
    } = values;

    try {
      const result = await login(email, password);


      // If result is not undefined then an error has been thrown
      if (result.data.token !== undefined) {
        // If token already exists, remove it
        if (cookie.load('userData')) {
          cookie.remove('userData', { path: '/' });
        }

        try {
          const details = await getAuthenticatedUserDetails(result.data.token);


          if (details.email !== undefined) {
            const userData = {
              token: result.data.token,
              email: details.email,
              firstName: details.firstName,
              lastName: details.lastName,
              userId: details.userId,

            };

            cookie.save('userData', JSON.stringify(userData), {
              path: '/',
              // expires,
              maxAge: 7200,
              secure: false,
            });
            setSubmitting(false);
            history.push('/');
          }
        } catch (error) {
          console.error(error);
        }
      }
    } catch (error) {
      console.error(error);
    }
  },

  displayName: 'LoginForm', // helps with React DevTools
})(MyForm);

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    return (

      <div>
        <BootstrapNavbar />
        <div
          className="container-fluid"
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <MyLoginEnhancedForm {...this.props} />
        </div>
      </div>

    );
  }
}

export default Login;
