import React, { useState } from 'react';
import { withFormik, Field } from 'formik';
import * as Yup from 'yup';
import { register } from '../services/todoListAPI';

import BootstrapNavbar from '../components/BootstrapNavbar';


class MyForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const {
      value, error, touched, label, className,
    } = this.props;

    return (
      <form onSubmit={this.props.handleSubmit}>
        <h1>Register </h1>
        <div className="form-group">
          <label
            htmlFor="email"
            style={{ display: 'block', marginTop: '.5rem' }}
          >
            <strong>Email</strong>
          </label>
          <input
            id="email"
            autoComplete="email"
            placeholder="Enter your email"
            type="email"
            value={this.props.values.email}
            onChange={this.props.handleChange}
            onBlur={this.props.handleBlur}
            className="form-control"
          />
          {this.props.errors.email && this.props.touched.email && (
          <div style={{ color: 'red', marginTop: '.5rem' }}>
            {this.props.errors.email}
          </div>
          )}
        </div>
        <div className="row">
          <div className="col-md-6">
            <div className="form-group">
              <label
                htmlFor="new-password"
                style={{ display: 'block', marginTop: '.5rem' }}
              >
                <strong>Password</strong>
              </label>
              <input
                id="password"
                autoComplete="password"
                placeholder="Enter your password"
                type="password"
                value={this.props.values.password}
                onChange={this.props.handleChange}
                onBlur={this.props.handleBlur}
                className="form-control"
              />
              {this.props.errors.password && this.props.touched.password && (
              <div style={{ color: 'red', marginTop: '.5rem' }}>
                {this.props.errors.password}
              </div>
              )}
            </div>
          </div>
          <div className="col-md-6">
            <div className="form-group">
              <label
                htmlFor="retypePassword"
                style={{ display: 'block', marginTop: '.5rem' }}
              >
                <strong>Retype Password</strong>
              </label>
              <input
                id="retypePassword"
                placeholder="Retype your password"
                type="password"
                value={this.props.values.retypePassword}
                onChange={this.props.handleChange}
                onBlur={this.props.handleBlur}
                className="form-control"
              />
              {this.props.errors.retypePassword
                            && this.props.touched.retypePassword && (
                            <div style={{ color: 'red', marginTop: '.5rem' }}>
                              {this.props.errors.retypePassword}
                            </div>
              )}
            </div>
          </div>

          <div className="col-md-6">
            <div className="form-group">
              <label
                htmlFor="firstName"
                style={{ display: 'block', marginTop: '.5rem' }}
              >
                <strong>First Name</strong>
              </label>
              <input
                id="firstName"
                placeholder="First Name"
                type="text"
                value={this.props.values.firstName}
                onChange={this.props.handleChange}
                onBlur={this.props.handleBlur}
                className="form-control"
              />
              {this.props.errors.firstName
                    && this.props.touched.firstName && (
                    <div style={{ color: 'red', marginTop: '.5rem' }}>
                      {this.props.errors.firstName}
                    </div>
              )}
            </div>
          </div>
          <div className="col-md-6">
            <div className="form-group">
              <label
                htmlFor="lastName"
                style={{ display: 'block', marginTop: '.5rem' }}
              >
                <strong>Last Name</strong>
              </label>
              <input
                id="lastName"
                placeholder="Last Name"
                type="text"
                value={this.props.values.lastName}
                onChange={this.props.handleChange}
                onBlur={this.props.handleBlur}
                className="form-control"
              />
              {this.props.errors.lastName
              && this.props.touched.lastName && (
              <div style={{ color: 'red', marginTop: '.5rem' }}>
                {this.props.errors.lastName}
              </div>
              )}
            </div>
          </div>

        </div>


        <button
          type="button"
          className="outline btn btn-default"
          onClick={this.props.handleReset}
          disabled={!this.props.dirty || this.props.isSubmitting}
        >
          Reset
        </button>
        <button
          type="submit"
          className="btn btn-primary"
          disabled={this.props.isSubmitting}
        >
          Submit
        </button>
      </form>
    );
  }
}

const MyRegisterEnhancedForm = withFormik({
  mapPropsToValues: (props) => ({
    email: '',
    password: '',
    retypePassword: '',
    firstName: '',
    lastName: '',
  }),
  validationSchema: Yup.object().shape({
    email: Yup.string()
      .email('Invalid email address')
      .required('Email is required'),
    password: Yup.string().required('Password is required').min(6).max(20),
    retypePassword: Yup.string()
      .oneOf([Yup.ref('password')], 'Passwords do not match')
      .required('Password retype is required'),
    firstName: Yup.string().required('First name is required'),
    lastName: Yup.string().required('Last name is required'),
  }),

  handleSubmit: async (values, { props, setSubmitting }) => {
    const { history } = props;

    const {
      email, password, firstName, lastName,
    } = values;

    try {
      const result = await register(email, password, firstName, lastName);
      // If result is not undefined then an error has been thrown
      if (result === undefined) {
        setSubmitting(false);
        history.push('/auth/login');
      }
    } catch (error) {
      console.error(error);
    }
  },

  displayName: 'RegisterForm',
})(MyForm);


class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div>
        <BootstrapNavbar />
        <div
          className="container-fluid"
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >

          <MyRegisterEnhancedForm {...this.props} />
        </div>
      </div>
    );
  }
}

export default Register;
