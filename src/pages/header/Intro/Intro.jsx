import React, { Component } from 'react';


class Intro extends Component {
  render() {
    return (
      <div id="intro" className="view">
        <div className="mask rgba-black-strong">
          <div id="desktop-header" className="container-fluid h-100">
            <div id="desktop-header2" className="row text-center" />
          </div>
        </div>
      </div>
    );
  }
}

export default Intro;
