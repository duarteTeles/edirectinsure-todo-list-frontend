import React from 'react';
import CardDeck from 'react-bootstrap/CardDeck';
import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
import { AiFillEdit, AiFillDelete } from 'react-icons/ai';
import cookie from 'react-cookies';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { withFormik } from 'formik';
import Tooltip from 'react-bootstrap/Tooltip';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import {
  getAuthenticatedUserDetails, getUserProjects, login, deleteProjectTask, getUserTasks, editProjectTask, editProject, newProjectTask,
} from '../../services/todoListAPI';


class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoggedIn: false,
      userId: '',
      userProjects: [],
      userTasks: [],
      value: '',
      newTaskState: '',
      newTaskDescription: '',
      projectName: '',
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleCreateNewProject = this.handleCreateNewProject.bind(this);
    this.handleCreateNewProjectTask = this.handleCreateNewProjectTask.bind(this);
    this.handleChangeTaskDescription = this.handleChangeTaskDescription.bind(this);
    this.handleChangeTaskState = this.handleChangeTaskState.bind(this);
  }

  async componentDidMount() {
    if (cookie.load('userData') !== undefined) {
      this.setState({ isLoggedIn: true, userId: cookie.load('userData').userId });
      const result = await getUserProjects(cookie.load('userData').token, cookie.load('userData').userId);
      this.setState({ userProjects: result.data });
      const allTasks = [];

      // update state with user tasks
      this.state.userProjects.forEach((project) => {
        const projectTasks = project.tasks;
        allTasks.push(projectTasks);
      });
      const allTasksMerged = allTasks.flat(1);
      this.setState({ userTasks: allTasksMerged });
    }
  }

  async handleProjectDelete(projectId) {
    const { userProjects } = this.state;
    const userProjectsCopy = userProjects;
    let flag = false;
    for (let i = 0; i < userProjectsCopy.length; i++) {
      if (userProjectsCopy[i].project_id === projectId) {
        userProjectsCopy.splice(i, 1);
        this.setState({ userProjects: userProjectsCopy });
        flag = true;
        alert('Project deleted successfully ');
        break;
      } else {
        flag = false;
      }
    }
    if (!flag) {
      alert('Failed to delete project');
    }
  }

  async handleProjectEdit(projectId) {
    const { userProjects } = this.state;
    const userProjectsCopy = userProjects;
    for (let i = 0; i < userProjectsCopy.length; i++) {
      if (userProjectsCopy[i].project_id === projectId) {
        const { userId } = userProjectsCopy[i];
        const newProjectName = 'Project new one';
        userProjectsCopy[i].name = newProjectName;
        this.setState({ userProjects: userProjectsCopy });
        try {
          await editProject(cookie.load('userData').token, userId, projectId, newProjectName);
          alert(`Project edited successfully with new name: ${newProjectName}`);
          break;
        } catch (error) {
          alert('Failed to edit project');
          console.error(error);
          break;
        }
      }
    }
  }

  handleChangeTaskDescription(event) {
    this.setState({ newTaskDescription: event.target.value });
  }

  handleChangeTaskState(event) {
    this.setState({ newTaskState: event.target.value });
  }

  async handleCreateNewProjectTask(event) {
    event.preventDefault();
    const newTask = {
      state: this.state.newTaskState,
      description: this.state.newTaskDescription,
    };
    alert(`A new task with ${newTask.state}, ${newTask.description} was simulated`);
  }

  //


  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  handleCreateNewProject(event) {
    event.preventDefault();


    const newProject = {
      name: this.state.value,
      tasks: [],
    };
    this.setState({
      userProjects: [...this.state.userProjects, newProject],
    });
    alert(`A new project was created with name: ${this.state.value}`);
  }

  async handleProjectTaskDelete(task) {
    const { userId, projectId } = task;


    const { userTasks } = this.state;

    const userTasksArrayCopy = userTasks;
    let flag = false;

    userTasksArrayCopy.forEach((userTask) => {
      if (userTask.projectId === projectId && userTask.userId === userId) {
        const indexToRemove = userTasksArrayCopy.indexOf(userTask);
        userTasksArrayCopy.splice(indexToRemove, 1);


        flag = true;
      }
    });


    if (flag) {
      try {
        await deleteProjectTask(cookie.load('userData').token, userId, projectId, task.task_id);
        this.setState({ userTasks: userTasksArrayCopy });
        alert('Task deleted successfully ');
      } catch (err) {
        console.error(err);
      }
    }

    if (!flag) {
      alert('Failed to delete task');
    }
  }

  async handleProjectTaskEdit(task) {
    const { userId, projectId } = task;
    const { userTasks } = this.state;
    const previousTaskState = task.state;
    task.state = 'done';
    const currentTaskState = task.state;


    try {
      await editProjectTask(cookie.load('userData').token, userId, projectId, task);
      alert(`Successfully changed task with ID ${task.taskId} from  ${previousTaskState} to ${currentTaskState}`);
    } catch (err) {
      console.error(err);
    }
  }


  render() {
    if (this.state.userProjects.length === 0 && this.state.isLoggedIn) {
      return 'Getting User Projects, please wait..';
    }
    function renderTooltip(taskFinishDate) {
      return (
        <Tooltip id="button-tooltip">
          {taskFinishDate}
        </Tooltip>
      );
    }

    const TaskToolTip = (taskFinishDate) => (

      <OverlayTrigger
        placement="right"
        delay={{ show: 250, hide: 400 }}
        overlay={renderTooltip(taskFinishDate)}
        arrowProps="test"
      >
        <Button variant="success">Hover to see task finish date!</Button>
      </OverlayTrigger>
    );


    const renderProjectCards = this.state.userProjects.map((project) => {
      const projectTasks = project.tasks;
      let flag;
      const { userTasks } = this.state;

      const renderTaskTextTodo = userTasks.map((task) => {
        if (task.state === 'todo' && task.projectId === project.project_id) {
          return (
            <Card.Text>
              {task.description}
              {' '}
              {TaskToolTip(task.finishDate)}
              {' '}
              <AiFillDelete style={{ marginLeft: 10 }} onClick={() => this.handleProjectTaskDelete(task)} />
              <AiFillEdit style={{ marginLeft: 10 }} onClick={() => this.handleProjectTaskEdit(task)} />
            </Card.Text>
          );
        }
      });
      const renderTaskTextDone = userTasks.map((task) => {
        if (task.state === 'done' && task.projectId === project.project_id) {
          return (
            <Card.Text style={{ background: '#ccc', textDecoration: 'line-through' }}>
              {task.description}
              {' '}
              {TaskToolTip(task.finishDate)}
              {' '}
            </Card.Text>
          );
        }
      });
      return (
        <Card>
          <div style={{ marginLeft: 20, marginTop: 20 }}>
            <AiFillEdit style={{ marginLeft: 10 }} onClick={() => this.handleProjectEdit(project.project_id)} />
            <AiFillDelete style={{ marginLeft: 10 }} onClick={() => this.handleProjectDelete(project.project_id)} />
          </div>
          <Card.Header>
            Project
            {' '}
            {this.state.projectName.length === 0 ? project.name : this.state.projectName}
          </Card.Header>
          <Card.Body>
            {/* FIXME: use flexbox to properly put icons on the right side of the card */}
            <Card.Title>Done</Card.Title>
            {renderTaskTextDone}
            <Card.Title>TODO</Card.Title>
            {renderTaskTextTodo}
          </Card.Body>
          <Card.Footer>
            <form onSubmit={this.handleCreateNewProjectTask}>

              <label>
                State:
                <input type="text" name="state" value={this.state.newTaskState} onChange={this.handleChangeTaskState} />
              </label>
              <label>
                Description:
                <input type="text" name="description" value={this.state.newTaskDescription} onChange={this.handleChangeTaskDescription} />
              </label>
              <button
                type="submit"
                className="btn btn-primary"
                style={{ marginLeft: 20 }}
              >
                Add Task
              </button>
            </form>
          </Card.Footer>
        </Card>

      );
    });


    return (
      <div>
        <main className="mt-5">
          <Container>
            {this.state.isLoggedIn ? (
              <div style={{ marginBottom: 20 }}>
                <Card>
                  <Card.Header>
                    Create a new project
                  </Card.Header>
                  <Card.Body>
                    <Card.Text>
                      <form onSubmit={this.handleCreateNewProject}>

                        <input type="text" value={this.state.value} onChange={this.handleChange} />
                        <button
                          type="submit"
                          className="btn btn-primary"
                          style={{ marginLeft: 20 }}
                        >
                          Create Project
                        </button>
                      </form>
                    </Card.Text>
                  </Card.Body>
                </Card>

              </div>
            ) : null}


            <div style={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap' }}>
              {this.state.isLoggedIn ? (
                <CardDeck>
                  {renderProjectCards}
                </CardDeck>
              ) : <h2> Please login to view your tasks and projects</h2>}
            </div>
          </Container>
        </main>
      </div>
    );
  }
}

Main.propTypes = {};

export default Main;
