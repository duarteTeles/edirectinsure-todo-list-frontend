import React from 'react';
import '../App.css';

import Button from 'react-bootstrap/Button';
import { login } from '../services/todoListAPI';
import BootstrapNavbar from '../components/BootstrapNavbar';
import Header from './header/Header';
import Main from './main/Main';
import Footer from './footer/Footer';


class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  async componentDidMount() {
    document.title = 'Home - EdirectInsure ToDo List';
  }

  render() {
    return (
      <header>
        <div>
          {/* Navbar  */}
          <BootstrapNavbar />
          {/* Header = introduction  */}
          <Header />
          {/* main content, including all the sections */}
          <Main />
          {/* Footer */}
          {/* <Footer /> */}
        </div>
      </header>
    );
  }
}

export default Home;
