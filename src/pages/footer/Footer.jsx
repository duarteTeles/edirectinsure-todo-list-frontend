import React from 'react';

const Footer = (props) => (
  <div>
    {/* Footer */}
    <footer className="page-footer font-small pt-0" id="footer-background" />
  </div>
);

export default Footer;
