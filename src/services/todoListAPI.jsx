import axios from 'axios';
import React from 'react';


const BACKEND_SERVER_NAME = 'localhost';
const BACKEND_SERVER_PORT = '3000';
const BACKEND_BASE_URL = 'api/users/';


const todoAPI = axios.create({
  baseURL: `http://${BACKEND_SERVER_NAME}:${BACKEND_SERVER_PORT}/${BACKEND_BASE_URL}`,
  timeout: 1000,
  headers: {
    contentType: 'application/json',
  },
});

const login = async (email, password) => {
  try {
    const result = await todoAPI.post('/login', {
      timeout: 5000,
      email,
      password,
    });
    return result;
  } catch (error) {
    if (error.response) {
      alert(JSON.stringify(error.response.data));
    } else {
      console.error(error);
    }
    return error.message;
  }
};

const getAuthenticatedUserDetails = async (token) => {
  try {
    const details = await todoAPI.get('/details', {
      timeout: 5000,
      headers: { token },
    });
    return details.data;
  } catch (error) {
    if (error.response) {
      alert(JSON.stringify(error.response.data));
    } else {
      console.error(error);
    }
    return error.message;
  }
};

const register = async (email, password, firstName, lastName) => {
  try {
    await todoAPI.post('/register', {
      timeout: 5000,
      email,
      password,
      firstName,
      lastName,
    });
    alert(`User ${firstName} ${lastName} successfully registered`);
  } catch (error) {
    if (error.response) {
      alert(JSON.stringify(error.response.data));
    } else {
      console.error();
    }
    return error.message;
  }
};

const getUserProjects = async (token, userId) => {
  try {
    const result = await todoAPI.get(`${userId}/projects/`, {
      timeout: 5000,
      headers: { token },
    });
    return result;
  } catch (error) {
    if (error.response) {
      alert(JSON.stringify(error.response.data));
    } else {
      console.error(error);
    }
    return error.message;
  }
};

const getUserTasks = async (token, userId, projectId) => {
  try {
    const result = await todoAPI.get(`${userId}/projects/${projectId}/tasks`, {
      timeout: 5000,
      headers: { token },
    });
    return result;
  } catch (error) {
    if (error.response) {
      alert(JSON.stringify(error.response.data));
    } else {
      console.error(error);
    }
    return error.message;
  }
};

const deleteProjectTask = async (token, userId, projectId, taskId) => {
  try {
    const result = await todoAPI.delete(`${userId}/projects/${projectId}/tasks/${taskId}`, {
      timeout: 5000,
      headers: { token },
    });
  } catch (error) {
    if (error.response) {
      alert(JSON.stringify(error.response.data));
    } else {
      console.error(error);
    }
    return error.message;
  }
};

const editProjectTask = async (token, userId, projectId, task) => {
  const {
    state, description, taskId,
  } = task;
  const config = {
    headers: {
      token,
    },
  };

  const data = {
    state: 'todo', description, finishDate: new Date(),
  };


  try {
    await todoAPI.put(`${userId}/projects/${projectId}/tasks/${taskId}`, data, config);
  } catch (error) {
    if (error.response) {
      alert(JSON.stringify(error.response.data));
    } else {
      console.error(error);
    }
    return error.message;
  }
};

const editProject = async (token, userId, projectId, projectName) => {
  const config = {
    headers: {
      token,
    },
  };

  try {
    await todoAPI.put(`${userId}/projects/${projectId}`, { name: projectName }, config);
  } catch (error) {
    if (error.response) {
      alert(JSON.stringify(error.response.data));
    } else {
      console.error(error);
    }
    return error.message;
  }
};

const newProjectTask = async (token, userId, projectId, task) => {
  const {
    state, description, taskId,
  } = task;
  const config = {
    headers: {
      token,
    },
  };


  try {
    await todoAPI.post(`${userId}/projects/${projectId}/tasks/${taskId}`, { state, description, finishDate: new Date() }, config);
  } catch (error) {
    if (error.response) {
      alert(JSON.stringify(error.response.data));
    } else {
      console.error(error);
      alert(error);
    }
    return error.message;
  }
};


export {
  login, register, getAuthenticatedUserDetails, getUserProjects, deleteProjectTask, getUserTasks, editProjectTask, editProject, newProjectTask,
};
